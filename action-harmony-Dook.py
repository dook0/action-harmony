#!/usr/bin/env python2
# -*-: coding utf-8 -*-

from hermes_python.hermes import Hermes
from snipshelpers.thread_handler import ThreadHandler
from hermes_python.ontology import *
import os


import Queue
import requests
import urllib2
import json

CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"

MQTT_IP_ADDR = "localhost"
MQTT_PORT = 1883
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT))

HARMONY_ADDR_TV = "http://192.168.1.57:8282/hubs/harmony-hub/devices/sony-tv/commands/"
HARMONY_ADDR_SONOS = "http://192.168.1.57:8282/hubs/harmony-hub/devices/"
HARMONY_ADDR_aTV = "http://192.168.1.57:8282/hubs/harmony-hub/devices/apple-tv/commands/"
HARMONY_ADDR_PS4 = "http://192.168.1.57:8282/hubs/harmony-hub/devices/sony-ps4/commands/"

class Skill:

    def __init__(self):
        self.queue = Queue.Queue()
        self.thread_handler = ThreadHandler()
        self.thread_handler.run(target=self.start_blocking)
        self.thread_handler.start_run_loop()

    def start_blocking(self, run_event):
        while run_event.is_set():
            try:
                self.queue.get(False)
            except Queue.Empty:
                with Hermes(MQTT_ADDR) as h:
                    h.subscribe_intents(self.callback).start()

    def extract_house_rooms(self, intent_message):
        house_rooms = []
        if intent_message.slots.house_room is not None:
            for room in intent_message.slots.house_room:
                house_rooms.append(room.slot_value.value.value)
        return house_rooms

    def extract_appliance(self, intent_message):
        appliancess = []
        if intent_message.slots.appliances is not None:
            for appliance in intent_message.slots.appliances:
                appliancess.append(appliance.slot_value.value.value)
        return appliancess

    def extract_command(self, intent_message):
        commandss = []
        if intent_message.slots.commands is not None:
            for command in intent_message.slots.commands:
                commandss.append(command.slot_value.value.value)
        return commandss

    def callback(self, hermes, intent_message):

        appliances = self.extract_appliance(intent_message)
        commands = self.extract_command(intent_message)
        house_rooms = self.extract_house_rooms(intent_message)

        if intent_message.intent.intent_name == 'Dook:turnOnAppliance':
            self.queue.put(self.appliance_turn_on(hermes, intent_message, appliances, house_rooms))
        if intent_message.intent.intent_name == 'Dook:turnOffAppliance':
            self.queue.put(self.appliance_turn_off(hermes, intent_message, appliances, house_rooms))
        if intent_message.intent.intent_name == 'Dook:controlAppliance':
            self.queue.put(self.appliance_control(hermes, intent_message, appliances, commands))

    def appliance_turn_off(self, hermes, intent_message, appliances, house_rooms):
        if len(appliances) > 0:
            if(appliances[0] == "television"):
                r = requests.post(HARMONY_ADDR_TV+"power-off")
                result_sentence = "J'\xc3\xa9teins la t\xc3\xa9l\xc3\xa9"
            if(appliances[0] == "musique"):

                if len(house_rooms) == 0:
                    r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/pause")
                    r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/pause")
                    result_sentence = "J'arr\xc3\xa9te la musique"
                else :
                    if house_rooms[0] == "chambre":
                        r = requests.post(HARMONY_ADDR_SONOS+house_rooms[0]+"/commands/pause")
                        result_sentence = "J'arr\xc3\xa9te la musique de la chambre"
                    elif house_rooms[0] == "salon":
                        r = requests.post(HARMONY_ADDR_SONOS+house_rooms[0]+"/commands/pause")
                        result_sentence = "J'arr\xc3\xa9te la musique du salon"
                    else :
                        r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/pause")
                        r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/pause")
                        result_sentence = "J'arr\xc3\xa9te la musique"

            if(appliances[0] == "appleTV"):
                r = requests.post(HARMONY_ADDR_aTV+"menu")
                result_sentence = "J'arr\xc3\xa9te l'apple T.V"
            if(appliances[0] == "playstation"):
                os.system("sudo /usr/local/bin/ps4-waker standby")
                result_sentence = "J'arr\xc3\xa9te la console"
                # r = requests.post(HARMONY_ADDR_PS4+"power-off")
        hermes.publish_end_session(intent_message.session_id, result_sentence)

    def appliance_turn_on(self, hermes, intent_message, appliance, house_rooms):
        if len(appliance) > 0:
            if(appliance[0] == "television"):
                r = requests.post(HARMONY_ADDR_TV+"power-on")
                result_sentence = "J'allume la t\xc3\xa9l\xc3\xa9"
            if(appliance[0] == "musique"):
                if len(house_rooms) == 0:
                    r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/play")
                    r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/play")
                    result_sentence = "Je vous mets de la musique"
                else :
                    if house_rooms[0] == "chambre":
                        r = requests.post(HARMONY_ADDR_SONOS+house_rooms[0]+"/commands/play")
                        result_sentence = "Je vous mets de la musique dans la chambre"
                    elif house_rooms[0] == "salon":
                        r = requests.post(HARMONY_ADDR_SONOS+house_rooms[0]+"/commands/play")
                        result_sentence = "Je vous mets de la musique dans le salon"
                    else :
                        r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/play")
                        r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/play")
                        result_sentence = "Je vous mets de la musique"

            if(appliance[0] == "appleTV"):
                r = requests.post(HARMONY_ADDR_aTV+"menu")
                result_sentence = "J'allume l'apple T.V."
            if(appliance[0] == "playstation"):
                os.system("sudo /usr/local/bin/ps4-waker")
                result_sentence = "Je vous allume la console"
                # r = requests.post(HARMONY_ADDR_PS4+"ps")
            if(appliance[0] == "film"):
                r = requests.post(HARMONY_ADDR_TV+"netflix")
                result_sentence = "Je vous met Netflixe"

        hermes.publish_end_session(intent_message.session_id, result_sentence)


    def appliance_control(self, hermes, intent_message, appliances, commands):
        if len(appliances) > 0:
            if appliances[0] == "film" and commands[0] =="pause":
                r = requests.post(HARMONY_ADDR_TV+"pause")
                result_sentence = "je mets pause"
            if appliances[0] == "film" and commands[0] =="continue":
                r = requests.post(HARMONY_ADDR_TV+"play")
                result_sentence = "Aller ! on reprend"


            if appliances[0] == "sound" and commands[0] =="baisser":
                r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/volume-down")
                r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/volume-down")

            if appliances[0] == "sound" and commands[0] =="monter":
                r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/volume-up")
                r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/volume-up")


            if appliances[0] == "song":
                if commands[0] =="previous":
                    r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/skip-backward")
                    r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/skip-backward")

                elif commands[0] =="next":
                    r = requests.post(HARMONY_ADDR_SONOS+"chambre"+"/commands/skip-forward")
                    r = requests.post(HARMONY_ADDR_SONOS+"salon"+"/commands/skip-forward")

                else :
                    print("nothing")

            # if appliances[0] == "musique" and commands[0] =="continue":
            #     r = requests.post(HARMONY_ADDR_SONOS+"chambre"+)
            #     result_sentence = "Je reprends la musique"

            if appliances[0] == "playstation" and commands[0] =="pause":
                r = requests.post(HARMONY_ADDR_PS4+"option")
                result_sentence = "Je suspends la console"

            if appliances[0] == "playstation" and commands[0] =="continue":
                r = requests.post(HARMONY_ADDR_PS4+"option")
                result_sentence = "on reprend le jeu"
        # if len(appliance) > 0:
        # else:
        # hermes.publish_end_session(intent_message.session_id, result_sentence)
        hermes.publish_end_session(intent_message.session_id, result_sentence)

if __name__ == "__main__":
    Skill()
